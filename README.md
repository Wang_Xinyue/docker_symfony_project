# Install PHP 7.4 on CentOS 7

#### Add EPEL and REMI Repository

```
sudo yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

sudo yum -y install https://rpms.remirepo.net/enterprise/remi-release-7.rpm
```

#### We can now enable PHP 7.4 Remi repository and install PHP 7.4 on CentOS 7.

```
sudo yum -y install yum-utils
sudo yum-config-manager --enable remi-php74
```

```
sudo yum update
sudo yum install php php-cli
```

#### Use the next command to install additional packages:

```
sudo yum install php  php-cli php-fpm php-mysqlnd php-zip php-devel php-gd php-mcrypt php-mbstring php-curl php-xml php-pear php-bcmath php-json
```

```
php -v
php --modules
```

# Install Git on CentOS 7 

```
yum install git
git --version
```

# Installing Composer on CentOS 

```
sudo yum install php-cli php-zip wget unzip

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

HASH="$(wget -q -O - https://composer.github.io/installer.sig)"

php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

composer -V
```

# Installing & Setting up the Symfony on CentOS

```
echo '[symfony-cli]
name=Symfony CLI
baseurl=https://repo.symfony.com/yum/
enabled=1
gpgcheck=0' | sudo tee /etc/yum.repos.d/symfony-cli.repo

sudo yum install symfony-cli

sudo symfony check:requirements

symfony new myproj --webapp
```


#### permission denied in VS Code MAC

```
sudo chown -R vagrant gitlab/*
```

# Install Docker Engine on CentOS

#### Uninstall old versions

```
sudo yum remove docker \
                  docker-client \
                  docker-client-latest \
                  docker-common \
                  docker-latest \
                  docker-latest-logrotate \
                  docker-logrotate \
                  docker-engine
```				  
				  
#### Install using the repository

```
sudo yum install -y yum-utils
 
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```

#### Install the latest version of Docker Engine, containerd, and Docker Compose or go to the next step to install a specific version:

```
sudo yum install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

# Installing Docker Compose

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

sudo docker-compose up -d

docker-compose --version
```
