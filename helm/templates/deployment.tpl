{{- define "deployment.template" -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: {{ .project.namespace | default "default" }}
  name: {{ .project.name | quote }}
spec:
  selector:
    matchLabels:
      service: {{ .project.name | quote }}
  template:
    metadata:
      labels:
        service: {{ .project.name | quote }}
    spec:
      containers:
        - name: {{ .project.name | quote }}
          image: {{ printf "%s:%s" .project.image .imageTag | quote }}
          imagePullPolicy: {{ .project.pullPolicy | default "IfNotPresent" | quote }}
{{- end -}}
